## Consolation 

* **Category:** Web
* **Points:** 50

### Challenge
```
I've been feeling down lately... Cheer me up!

Author: JoshDaBosh
```

### Solution
* **Author(s):** b0uldrr 

We're provided with a webpage with a single button with the text "Pay me some money". Everytime you press it, a counter next to the button increases by $25. Examining the source code, the button calls a function named nofret(). There is only one javascript file linked to the page, iftenmillionfireflies.js.

In the developer feature of Firefox, I opened the sources tab and examined the javascript file, which was obfuscated, and found the nofret() function: 

```
nofret() {
	document[_0x4229('0x95','kY1#')](_0x4229('0x9','kY1#'))[_0x4229('0x32','yblQ')]=parseInt(document[_0x4229('0x5e','xtR2')](_0x4229('0x2d','uCq1'))['innerHTML'])+0x19;
	console[_0x4229('0x14','70CK')](_0x4229('0x38','rwU*'));
	console['clear']();
}
```

While it's hard to tell what this function does, we can see it's adding 25 to the counter (0x19), writing something to the console and then clearing the console. I tried editing the javascript file directly in the developer tab to remote the console clear command, but I couldn't get this to work. Instead, I downloaded the entire page and support files, then edited the local javascript file to remove the console clear command, and opened the webpage locally. I opened the console in the developer tab, clicked the single button on the page, and the flag was written to the console.

### Flag 
```
actf{you_would_n0t_beli3ve_your_eyes}
```
