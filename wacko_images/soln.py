#!/usr/bin/python3

from numpy import *
from PIL import Image

enc = Image.open(r"enc.png")
img = array(enc)

key = [41, 37, 23]

a, b, c = img.shape

for x in range (0, a):
    for y in range (0, b):
        pixel = img[x, y]
        for i in range(0,3):

            # try every possible pixel value (0-256) until we find one that could have fit encoding algorithm
            for j in range(0,256):
                if j * key[i] % 251 == pixel[i]:
                    pixel[i] = j
                    break

        img[x][y] = pixel

flag = Image.fromarray(img)
flag.save('flag.png')
