## Wacko Images 

* **Category:** Crypto
* **Points:** 90
* **Author(s):** b0uldrr

---

### Challenge
```
How to make hiding stuff a e s t h e t i c? And can you make it normal again? enc.png image-encryption.py

Author: floorthfloor
```

### Downloads
* [encoded image](enc.png)
* [encryptor](image-encryption.py)

---

### Solution

We're provided with an image, "enc.png", and a python program, "image-encryption.py".

![enc](enc.png)

```
from numpy import *
from PIL import Image

flag = Image.open(r"flag.png")
img = array(flag)

key = [41, 37, 23]

a, b, c = img.shape

for x in range (0, a):
    for y in range (0, b):
        pixel = img[x, y]
        for i in range(0,3):
            pixel[i] = pixel[i] * key[i] % 251
        img[x][y] = pixel

enc = Image.fromarray(img)
enc.save('enc.png')
```

This script takes an image, "flag.png", and loops through every pixel, multiplying its value with a value from the key list, modded by 251. It then saves that image as enc.png.

I wrote a script to reverse that process:

```
#!/usr/bin/python3

from numpy import *
from PIL import Image

enc = Image.open(r"enc.png")
img = array(enc)

key = [41, 37, 23]

a, b, c = img.shape

for x in range (0, a):
    for y in range (0, b):
        pixel = img[x, y]
        for i in range(0,3):

            # try every possible pixel value (0-256) until we find one that could have fit encoding algorithm
            for j in range(0,256):
                if j * key[i] % 251 == pixel[i]:
                    pixel[i] = j
                    break

        img[x][y] = pixel

flag = Image.fromarray(img)
flag.save('flag.png')
```

The resulting image displayed the flag.

![flag](flag.png)

---

### Flag
```
actf{m0dd1ng_sk1llz}
```
