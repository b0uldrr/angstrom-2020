## Inputter 

* **Category:** Misc
* **Points:** 100
* **Author(s):** b0uldrr

---

### Challenge
```
Clam really likes challenging himself. When he learned about all these weird unprintable ASCII characters he just HAD to put it in a challenge. Can you satisfy his knack for strange and hard-to-input characters? [Source](inputter.c).

Find it on the shell server at /problems/2020/inputter/.

Author: aplet123

```
### Hints
There are ways to run programs without using the shell.

---

### Solution

We're provided with a binary and it's source, but to get the flag we need to conduct the attack on the provided shell server.

Looking at the source code of inputter.c, we can see that the main function performs a number of checks before determining whether to print the flag. It checks:
1. We have provided one argument to the program 
1. That argument is  \n'\"\x07
1. It then prompts for an input and checks that our response is \x00\x01\x02\x03\n

```
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define FLAGSIZE 128

void print_flag() {
    gid_t gid = getegid();
    setresgid(gid, gid, gid);
    FILE *file = fopen("flag.txt", "r");
    char flag[FLAGSIZE];
    if (file == NULL) {
        printf("Cannot read flag file.\n");
        exit(1);
    }
    fgets(flag, FLAGSIZE, file);
    printf("%s", flag);
}

int main(int argc, char* argv[]) {
    setvbuf(stdout, NULL, _IONBF, 0);
    if (argc != 2) {
        puts("Your argument count isn't right.");
        return 1;
    }
    if (strcmp(argv[1], " \n'\"\x07")) {
        puts("Your argument isn't right.");
        return 1;
    }
    char buf[128];
    fgets(buf, 128, stdin);
    if (strcmp(buf, "\x00\x01\x02\x03\n")) {
        puts("Your input isn't right.");
        return 1;
    }
    puts("You seem to know what you're doing.");
    print_flag();
}
```

The problem with the argument and input is that they are not printable characters that we can type. If we were provided with a remote sever, we could pipe in bytes with a python script but we have to connect to the shell server, which makes this tricker. My solution was to connect to the shell server and then run python interactively:

```
team5999@actf:/problems/2020/inputter$ python3
Python 3.5.2 (default, Oct  8 2019, 13:06:37) 
[GCC 5.4.0 20160609] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import subprocess
>>> conn = subprocess.run(['./inputter', " \n'\"\x07"], input=b'\x00\x01\x02\x03\n')
You seem to know what you're doing.
actf{impr4ctic4l_pr0blems_c4ll_f0r_impr4ctic4l_s0lutions}
>>> 
```
---

### Flag
```
actf{impr4ctic4l_pr0blems_c4ll_f0r_impr4ctic4l_s0lutions}
```
