#!/usr/bin/python3

import pwn

conn = pwn.remote('misc.2020.chall.actf.co', 20300)

# The first 50 fibonacci numbers
fibonacci = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269, 2178309, 3524578, 5702887, 9227465, 14930352, 24157817, 39088169, 63245986, 102334155, 165580141, 267914296, 433494437, 701408733, 1134903170, 1836311903, 2971215073, 4807526976, 7778742049]


# Rotate characters - Uppercase only!
def caesar(text,num_to_rotate):
    result = ""

    # For every character...
    for char in text:

        rotated_char = ord(char) + (num_to_rotate % 26)
        if  rotated_char > 90:
            rotated_char = (rotated_char - 90) + 64 

        result += chr(rotated_char)

    return result


# We know we have to solve 50 problems
for i in range(50):

    # Extract the p value, removing the space at the end
    conn.recvuntil("Shift ")
    text = conn.recvuntil(" ")[:-1]

    # Extract the n value, removing the newline char at the end
    conn.recvuntil("n=")
    num_to_rotate = conn.recvuntil("\n")[:-1]

    # Work out the answer
    answer = caesar(text.decode(), fibonacci[int(num_to_rotate)])

    # Print some debugging output
    print(i,end="")
    print(".")
    print("p =", text.decode())
    print("n =", int(num_to_rotate))
    print("Answer =", answer)
    print()

    # Send the answer
    conn.sendline(answer)

# After 50 solves, print the flag
print(conn.recvall())
