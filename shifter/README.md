## Shifter 

* **Category:** Misc
* **Points:** 160
* **Author(s):** b0uldrr

---

### Challenge
```
What a strange challenge...
It'll be no problem for you, of course!
nc misc.2020.chall.actf.co 20300

Author: JoshDaBosh
```
### Hints
Do you really need to calculate all those numbers?

---

### Solution

Upon connecting to the remote sever, we're told we'll be given a number 'n' and a plaintext 'p'. Our job is to Caesar shift p by the nth Fibonacci number. n will always be less than 50. We have a timelimit of 60 seconds to solve 50 challenges.

```
tmp@localhost:~/ctf/angstrom/shifter$ nc misc.2020.chall.actf.co 20300
Solve 50 of these epic problems in a row to prove you are a master crypto man like Aplet123!
You'll be given a number n and also a plaintext p.
Caesar shift `p` with the nth Fibonacci number.
n < 50, p is completely uppercase and alphabetic, len(p) < 50
You have 60 seconds!
--------------------
Shift HMMDIWCWHYWCFVAQR by n=5
: 
```

At first I tried calcluating the nth Fibonacci number on the fly but this was too slow, so instead I hardcoded the first 50 Fibonacci numbers into the python program. I used Wolfram Alpha to find the first 50 Fibonacci numbers, as I found it provided easiest output to copy and paste into my code. Here is my solution:

```
#!/usr/bin/python3

import pwn

conn = pwn.remote('misc.2020.chall.actf.co', 20300)

# The first 50 fibonacci numbers
fibonacci = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269, 2178309, 3524578, 5702887, 9227465, 14930352, 24157817, 39088169, 63245986, 102334155, 165580141, 267914296, 433494437, 701408733, 1134903170, 1836311903, 2971215073, 4807526976, 7778742049]


# Rotate characters - Uppercase only!
def caesar(text,num_to_rotate):
    result = ""

    # For every character...
    for char in text:

        rotated_char = ord(char) + (num_to_rotate % 26)
        if  rotated_char > 90:
            rotated_char = (rotated_char - 90) + 64 

        result += chr(rotated_char)

    return result


# We know we have to solve 50 problems
for i in range(50):

    # Extract the p value, removing the space at the end
    conn.recvuntil("Shift ")
    text = conn.recvuntil(" ")[:-1]

    # Extract the n value, removing the newline char at the end
    conn.recvuntil("n=")
    num_to_rotate = conn.recvuntil("\n")[:-1]

    # Work out the answer
    answer = caesar(text.decode(), fibonacci[int(num_to_rotate)])

    # Print some debugging output
    print(i,end="")
    print(".")
    print("p =", text.decode())
    print("n =", int(num_to_rotate))
    print("Answer =", answer)
    print()

    # Send the answer
    conn.sendline(answer)

# After 50 solves, print the flag
print(conn.recvall())
```

And the output:

```
tmp@localhost:~/ctf/angstrom/shifter$ ./soln.py 
[+] Opening connection to misc.2020.chall.actf.co on port 20300: Done
0.
p = DTHCCIFRS
n = 12
Answer = RHVQQWTFG

1.
p = AMQNWPGGMUKFBORHDM
n = 21
Answer = AMQNWPGGMUKFBORHDM

2.
p = GNGCYEGLKLJJCLFQCBRRQNYFKAJZVZYC
n = 2
Answer = HOHDZFHMLMKKDMGRDCSSROZGLBKAWAZD

...

47.
p = C
n = 13
Answer = B

48.
p = ROMOPZGIWYFJQIWVYK
n = 5
Answer = WTRTUELNBDKOVNBADP

49.
p = FNXLRD
n = 38
Answer = IQAOUG

[+] Receiving all data: Done (42B)
[*] Closed connection to misc.2020.chall.actf.co port 20300
b': actf{h0p3_y0u_us3d_th3_f0rmu14-1985098}\n'
```

---

### Flag
```
actf{h0p3_y0u_us3d_th3_f0rmu14-1985098}
```
