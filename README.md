## Angstrom CTF 2020

* **CTF Time page:** https://ctftime.org/event/982
* **Category:** Jeopardy
* **Date:** Sat, 14 March 2020, 00:00 UTC — Thu, 19 March 2020, 00:00 UTC

---

### Solved Challenges
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
|canary                      |pwn       |70      |bof, printf(), format string attack, canary|
|consolation                 |web       |50      |javascript, browser console|
|inputter                    |misc      |100     | |
|msd                         |misc      |140     |python, image manipulation, steg|
|no canary                   |pwn       |50      |bof|
|secret agents               |web       |110     |burp, SQL injection|
|shifter                     |misc      |160     |q&a coding|
|taking off                  |rev       |70      |static analysis|
|wacko images                |crypto    |90      |python, image manipulation, steg|
|xmas still stands           |web       |50      |Stored XSS, webhook, cookies|
