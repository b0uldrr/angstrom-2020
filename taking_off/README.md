## Taking Off 

* **Category:** Rev
* **Points:** 70
* **Author(s):** b0uldrr

---

### Challenge
```
So you started revving up, but is it enough to take off? Find the problem in /problems/2020/taking_off/ in the shell server.

Author: aplet123
```
### Hints
You should look into tools like GHIDRA, gdb and objdump.

### Downloads
* [taking off binary](taking_off)

---

### Solution

I downloaded the provided binary, 'taking_off'. Running it prints some text about having the correct arguments and then exits:

```
tmp@localhost:~/ctf/angstrom/taking_off$ ./taking_off 
So you figured out how to provide input and command line arguments.
But can you figure out what input to provide?
Make sure you have the correct amount of command line arguments!
```

I opened the program and Ghidra and analysed the main function. I changed the function signature of main to:

```
int main (int argc, char** argv)
```

This pretties up command line arguments in Ghidra. I found that hint in this video: https://www.youtube.com/watch?v=fTGTnrgjuGA&t=6m17s 

Here's the first part of the source code:

```
  if (argc == 5) {
    string_to_int(argv[1],&arg_1,&arg_1);
    string_to_int(argv[2],&arg_2,&arg_2);
    string_to_int(argv[3],&arg_3,&arg_3);
    iVar1 = is_invalid((ulong)arg_1);
    if (iVar1 == 0) {
      iVar1 = is_invalid((ulong)arg_2);
      if (iVar1 == 0) {
        iVar1 = is_invalid((ulong)arg_3);
        if ((iVar1 == 0) && (arg_3 + arg_2 * 100 + arg_1 * 10 == 0x3a4)) {
          iVar1 = strcmp(argv[4],"chicken");
          if (iVar1 == 0) {
	    // continued below
```

This code performs the following checks:
1. Do we have 4 command line arguments?
1. Are arguments 1, 2 and 3 integers greater than 0 and less than 9? (This is checked by the is_valid() function)
1. Do arguments 1, 2 and 3 satisfy this equation?: arg3 + arg2 * 100 + arg1 * 10 == 932
1. Is the 4th argument "chicken"

After some quick mental maths, we can satisfy these commandline arguments by calling the program with:

```
$ ./taking_off 3 9 2 chicken
```

But then the program prompts for a password. Looking further into the main function we find this code:

```
            puts("Well, you found the arguments, but what\'s the password?");
            fgets((char *)local_98,0x80,stdin);
            local_a0 = strchr((char *)local_98,10);
            if (local_a0 != (char *)0x0) {
              *local_a0 = '\0';
            }
            sVar2 = strlen((char *)local_98);
            local_a4 = (int)sVar2;
            local_a8 = 0;
            while (local_a8 <= local_a4) {
              if ((local_98[local_a8] ^ 0x2a) != desired[local_a8]) {
                puts("I\'m sure it\'s just a typo. Try again.");
                iVar1 = 1;
                goto LAB_00400bc7;
              }
              local_a8 = local_a8 + 1;
            }
            puts("Good job! You\'re ready to move on to bigger and badder rev!");
            print_flag();
            iVar1 = 0;
            goto LAB_00400bc7;
          }
        }
      }
    }
```
This section of code accepts user input and then loops through each character XOR'ing it against 0x2A, then comparing the result against the value in a local array named "desired".

Looking at the value of the desired array, we find these values:

```
5A 46 4F 4B 59 4F 0A 4D 43 5C 4F 0A 4C 46 4B 4D 2A
```

XOR'ing these values agaisnt 0x2A gives these results:

```
70 6c 65 61 73 65 20 67 69 76 65 20 66 6c 61 67 0a
```

Which when converted to ASCII spells "please give flag". This is the final check, so when we connect to the shell server and enter the above commandline arguments and that password, we are given the flag:

```
team5999@actf:/problems/2020/taking_off$ ./taking_off 3 9 2 chicken
So you figured out how to provide input and command line arguments.
But can you figure out what input to provide?
Well, you found the arguments, but what's the password?
please give flag
Good job! You're ready to move on to bigger and badder rev!
actf{th3y_gr0w_up_s0_f4st}
```

### Flag
```
actf{th3y_gr0w_up_s0_f4st}
```
