#!/usr/bin/python3
from PIL import Image

enc = Image.open('encoded.png')
org = Image.open('original.jpg')

width, height = enc.size

# To extract the MSB of a pixel, we compare the number of bits in the encoded pixel
# against the number of bits in the same pixel of the original image.
def extract_MSB(encoded_value, original_value):
    if len(encoded_value) == len(original_value):
        return encoded_value[0]
    else:
        return 0

# Loop through every pixel of both the encoded and original images at the same time. We know they're the same height & width
for h in range(height):
    for w in range(width):

        # encoded image R, G and B values
        enc_R, enc_G, enc_B = enc.getpixel((w,h))

        # original image R, G and B values
        org_R, org_G, org_B = org.getpixel((w,h))

        # turn our individual RGB values into lists so that we can extract the MSB
        enc_R = list(str(enc_R))
        enc_G = list(str(enc_G))
        enc_B = list(str(enc_B))

        org_R = list(str(org_R))
        org_G = list(str(org_G))
        org_B = list(str(org_B))

        # extract the MSB of the pixel and print it to screen
        print(extract_MSB(enc_R, org_R),end='')
        print(extract_MSB(enc_G, org_G),end='')
        print(extract_MSB(enc_B, org_B),end='')
