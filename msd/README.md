## MSD 

* **Category:** Misc
* **Points:** 140
* **Author(s):** b0uldrr

---

### Challenge
```
You thought Angstrom would have a stereotypical LSB challenge... You were wrong! To spice it up, we're now using the Most Significant Digit. Can you still power through it?
Here's the encoded image, and here's the original image, for the... well, you'll see.
Important: Redownload the image if you have one from before 11:30 AM 3/14/20. Important: Don't use Python 3.8, use an older version of Python 3!

Author: JoshDaBosh

```
### Hints
* Look at the difference between the original and what I created!
* Also, which way does LSB work?

### Downloads
* [original.png](original.png)
* [encoded.png](encoded.png)
* [public.py](public.py)

---

### Solution

We're given an image which has been encoded with the flag in the most-significate-digit of each pixel value. We're also provided with the original image and the script which was used to encode the flag inside it, although with the flag text redacted.

Encoded image:

![encoded image](encoded.png)

Original image:

![original image](original.jpg)

Encoding script (public.py):
```
from PIL import Image

im = Image.open('breathe.jpg')
im2 = Image.open("breathe.jpg")

width, height = im.size

flag = "REDACT"
flag = ''.join([str(ord(i)) for i in flag])


def encode(i, d):
    i = list(str(i))
    i[0] = d

    return int(''.join(i))
    
c = 0

for j in range(height):
    for i in range(width):
        data = []
        for a in im.getpixel((i,j)):
            data.append(encode(a, flag[c % len(flag)]))

            c+=1

        im.putpixel((i,j), tuple(data))
        
im.save("output.png")
pixels = im.load()
```
I wrote a script to extract those values from the encoded image. It prints the most significant digit to the screen.

soln.py:
```
#!/usr/bin/python3
from PIL import Image

enc = Image.open('encoded.png')
org = Image.open('original.jpg')

width, height = enc.size

# To extract the MSB of a pixel, we compare the number of bits in the encoded pixel
# against the number of bits in the same pixel of the original image.
def extract_MSB(encoded_value, original_value):
    if len(encoded_value) == len(original_value):
        return encoded_value[0]
    else:
        return 0

# Loop through every pixel of both the encoded and original images at the same time. We know they're the same height & width
for h in range(height):
    for w in range(width):

        # encoded image R, G and B values
        enc_R, enc_G, enc_B = enc.getpixel((w,h))

        # original image R, G and B values
        org_R, org_G, org_B = org.getpixel((w,h))

        # turn our individual RGB values into lists so that we can extract the MSB
        enc_R = list(str(enc_R))
        enc_G = list(str(enc_G))
        enc_B = list(str(enc_B))

        org_R = list(str(org_R))
        org_G = list(str(org_G))
        org_B = list(str(org_B))

        # extract the MSB of the pixel and print it to screen
        print(extract_MSB(enc_R, org_R),end='')
        print(extract_MSB(enc_G, org_G),end='')
        print(extract_MSB(enc_B, org_B),end='')
```

The output was a very long string of numbers. I knew the flag was going to start with actf{, so I searched for a sequence of 9799116102123, which are the ascii numbers for those characters. I found the sequence:

![ordinal output](images/ordinal_output.png)

I copied that chunk of text into an online decimal to ascii converter, prettied it up and found the flag:

![flag.png](images/flag.png)


### Flag
```
actf{inhale_exhale_ezpz-12309biggyhaby}
```
